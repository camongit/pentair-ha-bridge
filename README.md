# Pentair HA Bridge #

Docker image for communicating between node-screenlogic and Home Assistant Core

* This addon requires a MQTT broker to pass commands and circuit states between HomeAssistant Core and Screenlogic
* You need to know how many "Features" or "Circuits" you have enabled. Also if you have the intelliChem addon.

### How to ###

* Clone this repository to your server
* Modify the "send_state_to_ha.js" and "intitalize.js" and comment or uncomment any intelliChem sensors used
* Modify "runtime_variables.conf" with your MQTT configuration and screenlogic IP address.
* Build the docker image
* Launch the docker image
Example:
` docker -d -v /srv/configfiles/runtime_variables.conf:/pentair/runtime_variables.conf --name=pentair-ha-bridge camon/pentair-ha-bridge`
* You can check the logs of the container OR open a shell to the container and run:
`node /node_modules/node-screenlogic/initalize.js`
This will tell you the circuit ID for each circuit.
* Update your HA configuration with switches for each circuit and temperature/intelliChem sensors. See "ha_configuration_examples.yaml" for examples.

### Credits ###

This code is based on the original pentair-screenlogic addion written for hassio by [krk628](https://github.com/krk628/hassio-screenlogic-addon) and [bwoodworth](https://github.com/bwoodworth/hassio-addons/tree/master/pentair-screenlogic) and modified by myself for viable use in HomeAssistant Core.

MQTT Launcher was written by [jpmens](https://github.com/jpmens/mqtt-launcher) and modifed by myself for specific use.
