#!/bin/bash

## Continous monitor of Pentair Screenlogic device
## Original Developed by bwoodworth https://github.com/bwoodworth/hassio-addons/blob/master/pentair-screenlogic/initialize.js
## Built specifically for HomeAssistant CORE users. See  https://bitbucket.org/camongit/pentair-ha-bridge

#set -e

# Call config file
source /pentair/runtime_variables.conf

cd /node_modules/node-screenlogic

node initialize.js $screenlogic_ip

while [ 1 ]; do

# change IP address (-h) port (-p) username (-u) and password (-P) to match your MQTT broker settings
node send_state_to_ha.js $screenlogic_ip | awk -F, '{print "mosquitto_pub -h '"$mqtt_broker"' -p '"$mqtt_port"' -u '"$mqtt_username"' -P '"$mqtt_password"' -t " $1 " -m " $2}' | bash -s
sleep 1

done
