#!/usr/bin/env python3

import os
import sys
import subprocess
import logging
import paho.mqtt.client as paho   # pip install paho-mqtt
import time
import socket
import string

CONFIG=os.getenv('MQTTLAUNCHERCONFIG', '/pentair/runtime_variables.conf')

class Config(object):
    def __init__(self, filename=CONFIG):
        self.config = {}
        exec(compile(open(filename, "rb").read(), filename, 'exec'), self.config)

    def get(self, key, default=None):
        return self.config.get(key, default)

try:
    cf = Config()
except Exception as e:
    print ("Cannot load configuration from file %s: %s" % (CONFIG, str(e)))
    sys.exit(2)

topiclist = {

    # topic                     payload value       program & arguments
    "pentair/circuit/500/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '500', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '500', '0' ],
                            },
    "pentair/circuit/501/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '501', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '501', '0' ],
                            },
    "pentair/circuit/502/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '502', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '502', '0' ],
                            },
    "pentair/circuit/503/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '503', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '503', '0' ],
                            },
    "pentair/circuit/504/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '504', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '504', '0' ],
                            },
    "pentair/circuit/505/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '505', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '505', '0' ],
                            },
    "pentair/circuit/506/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '506', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '506', '0' ],
                            },
    "pentair/circuit/507/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '507', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '507', '0' ],
                            },
    "pentair/circuit/508/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '508', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '508', '0' ],
                            },
    "pentair/circuit/509/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '509', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '509', '0' ],
                            },
    "pentair/circuit/510/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '510', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '510', '0' ],
                            },
    "pentair/circuit/511/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '511', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '511', '0' ],
                            },
    "pentair/circuit/512/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '512', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '512', '0' ],
                            },
    "pentair/circuit/513/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '513', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '513', '0' ],
                            },
    "pentair/circuit/514/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '514', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '514', '0' ],
                            },
    "pentair/circuit/515/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '515', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '515', '0' ],
                            },
    "pentair/circuit/516/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '516', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '516', '0' ],
                            },
    "pentair/circuit/517/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '517', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '517', '0' ],
                            },
    "pentair/circuit/518/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '518', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '518', '0' ],
                            },
    "pentair/circuit/519/command"          :   {
                                'ON'        :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '519', '1' ],
                                'OFF'       :   [ '/node_modules/node-screenlogic/set_circuit.js', cf.get('screenlogic_ip'), '519', '0' ],
                            },
    "pentair/heater/spa/mode/set"          :   {
                                'heat'        :   [ '/node_modules/node-screenlogic/set_heater.js', cf.get('screenlogic_ip'), '1', '1' ],
                                'off'       :   [ '/node_modules/node-screenlogic/set_heater.js', cf.get('screenlogic_ip'), '1', '0' ],
                            },
    "pentair/heater/spa/temperature/set"          :   {
                                None        :   [ '/node_modules/node-screenlogic/set_heattemp.js', cf.get('screenlogic_ip'), '1', "@!@" ],
                            },
    "pentair/heater/pool/mode/set"          :   {
                                'heat'        :   [ '/node_modules/node-screenlogic/set_heater.js', cf.get('screenlogic_ip'), '0', '1' ],
                                'off'       :   [ '/node_modules/node-screenlogic/set_heater.js', cf.get('screenlogic_ip'), '0', '0' ],
                            },
    "pentair/heater/pool/temperature/set"          :   {
                                None        :   [ '/node_modules/node-screenlogic/set_heattemp.js', cf.get('screenlogic_ip'), '0', "@!@" ],
                            },
}
