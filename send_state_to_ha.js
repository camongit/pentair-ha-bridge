// Send state of Pentair to HomeAssistant via MQTT
// Original Developed by krk628 https://github.com/krk628/hassio-screenlogic-addon/blob/master/send_state_to_ha.js
// Secondary Development by bwoodworth https://github.com/bwoodworth/hassio-addons/blob/master/pentair-screenlogic/send_state_to_ha.js
// Built specifically for HomeAssistant CORE users. See  https://bitbucket.org/camongit/pentair-ha-bridge

'use strict';

const ScreenLogic = require('./index');

var myArgs = process.argv.slice(2);

connect(new ScreenLogic.UnitConnection(80, myArgs[0]));

function connect(client) {
  client.on('loggedIn', function() {
    this.getVersion();
  }).on('version', function(version) {
    this.getPoolStatus();
  }).on('poolStatus', function(status) {
    this.getChemicalData();
// Uncomment or Comment as needed for system
    console.log('pentair/pooltemp/state,' + status.currentTemp[0]);
    console.log('pentair/spatemp/state,' + status.currentTemp[1]);
    console.log('pentair/airtemp/state,' + status.airTemp);
    console.log('pentair/heater/spa/temperature/cstate,' + status.currentTemp[1]);
    console.log('pentair/heater/spa/mode/astate,' + (status.heatStatus[1] ? 'heating' : 'idle'));
    console.log('pentair/heater/spa/mode/pstate,' + (status.heatMode[1] ? 'heat' : 'off'));
    console.log('pentair/heater/spa/temperature/tstate,' + status.setPoint[1]);
    console.log('pentair/heater/pool/temperature/cstate,' + status.currentTemp[0]);
    console.log('pentair/heater/pool/mode/astate,' + (status.heatStatus[0] ? 'heating' : 'idle'));
    console.log('pentair/heater/pool/mode/pstate,' + (status.heatMode[0] ? 'heat' : 'off'));
    console.log('pentair/heater/pool/temperature/tstate,' + status.setPoint[0]);
    //console.log('/pentair/saltppm/state,' + status.saltPPM);
    //console.log('/pentair/ph/state,' + status.pH);
    //console.log('/pentair/saturation/state,' + status.saturation);
    var i;
    for(i = 0; i < status.circuitArray.length; i++)
    {
      console.log('pentair/circuit/' + status.circuitArray[i].id + '/state,' + (status.circuitArray[i].state ? 'ON' : 'OFF'));
    }
  }).on('chemicalData', function(chemData) {
// Uncomment as needed for system
    //console.log('/pentair/calcium/state,' + chemData.calcium);
    //console.log('/pentair/cyanuricacid/state,' + chemData.cyanuricAcid);
    //console.log('/pentair/alkalinity/state,' + chemData.alkalinity);
    client.close();
  });

  client.connect();
}
