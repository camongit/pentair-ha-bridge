#!/usr/bin/node
// Send command to set the temp of Pool or Spa heater. 
// Original Developed by bwoodworth https://github.com/bwoodworth/hassio-addons/blob/master/pentair-screenlogic/set_circuit
// Built specifically for HomeAssistant CORE users. See  https://bitbucket.org/camongit/pentair-ha-bridge

// Script executed by /pentair_scripts/mqtt_launcher.py

const ScreenLogic = require('./index');

var myArgs = process.argv.slice(2);

connect(new ScreenLogic.UnitConnection(80, myArgs[0]), parseInt(myArgs[1]), parseInt(myArgs[2]));

function connect(client, heater, temp) {
  client.on('loggedIn', function() {
    this.setSetPoint(0, heater, temp);
    client.close();
  });

  client.connect();
}
