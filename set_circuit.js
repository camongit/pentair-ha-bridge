#!/usr/bin/node
// Send commands to pentair from MQTT
// Original Developed by bwoodworth https://github.com/bwoodworth/hassio-addons/blob/master/pentair-screenlogic/set_circuit
// Built specifically for HomeAssistant CORE users. See  https://bitbucket.org/camongit/pentair-ha-bridge

// Script executed by mqtt_launcher.py

const ScreenLogic = require('./index');

var myArgs = process.argv.slice(2);

connect(new ScreenLogic.UnitConnection(80, myArgs[0]), parseInt(myArgs[1]), parseInt(myArgs[2]));

function connect(client, circuit, onoff) {
  client.on('loggedIn', function() {
    this.setCircuitState(0, circuit, onoff);
    client.close();
  });

  client.connect();
}
