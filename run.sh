#!/bin/bash

## Iitalization script for Pentair Screenlogic to HA interface
## Built specifically for HomeAssistant CORE users. See  https://bitbucket.org/camongit/pentair-ha-bridge

#set -e

# Call scripts
nohup /pentair_scripts/mqtt-launcher.py &
/pentair_scripts/monitor.sh
