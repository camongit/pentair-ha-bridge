FROM alpine

ENV LANG C.UTF-8

RUN apk add npm jq mosquitto-clients bash python3 py-pip && npm install node-screenlogic smart-buffer && pip install paho-mqtt

RUN mkdir /pentair_scripts
COPY run.sh /
COPY monitor.sh /pentair_scripts/
COPY mqtt-launcher.py /pentair_scripts/
COPY mqttlaunchercmds.py /pentair_scripts/
COPY send_state_to_ha.js /node_modules/node-screenlogic/
COPY initialize.js /node_modules/node-screenlogic/
COPY set_circuit.js /node_modules/node-screenlogic/
COPY set_heater.js /node_modules/node-screenlogic/
COPY set_heattemp.js /node_modules/node-screenlogic/

RUN chmod a+x /node_modules/node-screenlogic/*
RUN chmod a+x /pentair_scripts/*
RUN chmod a+x /run.sh

CMD [ "/run.sh" ]
