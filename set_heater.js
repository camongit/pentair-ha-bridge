#!/usr/bin/node
// Send command to change state of Pool or Spa heater. Note: THIS WILL ALSO TURN POOL OR SPA ON.
// Original Developed by bwoodworth https://github.com/bwoodworth/hassio-addons/blob/master/pentair-screenlogic/set_circuit
// Built specifically for HomeAssistant CORE users. See  https://bitbucket.org/camongit/pentair-ha-bridge

// Script executed by /pentair_scripts/mqtt_launcher.py

const ScreenLogic = require('./index');

var myArgs = process.argv.slice(2);

connect(new ScreenLogic.UnitConnection(80, myArgs[0]), parseInt(myArgs[1]), parseInt(myArgs[2]));

function connect(client, heater, onoff) {
  client.on('loggedIn', function() {
// Turn Spa or Pool modes on or off. Comment this if undesired
  if (heater == 0) {
    this.setCircuitState(0, 505, onoff);
  }else if (heater ==1) {
    this.setCircuitState(0, 500, onoff);
  }
// Turn Heater for Pool or Spa on or Off.
  if (onoff == 1) {
    this.setHeatMode(0, heater, 3);
  }else if (onoff ==0) {
    this.setHeatMode(0, heater, 0);
  }
    client.close();
  });

  client.connect();
}
